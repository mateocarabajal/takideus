from machine import Pin, I2C, PWM
from time import sleep
from machine import ADC
from dfplayermini import Player
import ssd1306
import config
from time import sleep

"""
#######################################################################
#     ____ ____ ____ ____ ____ ____ ____ ____ 
#    ||t |||a |||k |||i |||d |||e |||u |||s ||
#    ||__|||__|||__|||__|||__|||__|||__|||__||
#    |/__\|/__\|/__\|/__\|/__\|/__\|/__\|/__\|
#
#######################################################################

Para modificar los valores de la red, los nodos y el nombre de la terminal
hay que modificar el archivo config.py
"""


try:
    import usocket as socket
except:
    import socket
import network

def http_get(url):
    _, _, host, path = url.split('/', 3)
    addr = socket.getaddrinfo(host, 80)[0][-1]
    s = socket.socket()
    s.connect(addr)
    s.send(bytes('GET /%s HTTP/1.0\r\nHost: %s\r\n\r\n' % (path, host), 'utf8'))
    #while True:
    #    data = s.recv(100)
    #    if data:
    #        print(str(data, 'utf8'), end='')
    #    else:
    #        break
    s.close()

def web_page(nombre,color):

  
  html = """<html><head>
  <title>ESP Web Server</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="data:,">
  <style>html{font-family: Helvetica; display:inline-block; margin: 0px auto; text-align: center;}
  h1{color: """+color+"""; padding: 2vh;}p{font-size: 1.5rem;}.button{display: inline-block; background-color: """+color+"""; border: none; 
  border-radius: 4px; color: white; padding: 16px 40px; text-decoration: none; font-size: 30px; margin: 2px; cursor: pointer;}
  .button2{background-color: #4286f4;}</style>
  </head>
  <body> <h1>takideus</h1>
  <h2>""" + nombre + """</h2>
  <p><a href="/?musica=play"><button class="button play">play</button></a>
  <p><a href="/?musica=stop"><button class="button stop">stop</button></a></p>
  </body></html>"""#.encode('utf-8')
  return html

# Configuro la pantalla OLED
i2c = I2C(-1, scl=Pin(4), sda=Pin(5))
oled_width = 128
oled_height = 32
oled = ssd1306.SSD1306_I2C(oled_width, oled_height, i2c)





ssid = config.ssid
password = config.password
red = config.red
nodos = config.nodos
nombre = config.nombre
color = config.color
music = Player() # dfplayer mini

## Conectando al wifi ## 
print("inicio")
station = network.WLAN(network.STA_IF)
station.active(True)
station.connect(ssid, password)
oled.fill(0)
oled.text(nombre ,0, 0)
oled.text("conectando" ,0, 24)
oled.show()
while station.isconnected() == False:
    pass
ip2= station.ifconfig()
ip=ip2[0]
oled.fill(0)
oled.text("conexion ok" ,0, 0)
oled.text(ip,0,24)
oled.show()
print(ip)
print("conexion ok")
print(station.ifconfig())

## iniciando el socket y quedando a la espera de una conexion

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('', 80))
s.listen(5)

while True:
    conn, addr = s.accept()
    print('Got a connection from %s' % str(addr))
    request = conn.recv(1024)
    request = str(request)
    #print('Content = %s' % request)
    play = request.find('/?musica=play')
    stop = request.find('/?musica=stop')
    if play == 6:
        for ips in range(len(nodos)):
            redes = red + nodos[ips] + "/?musica=play"
            http_get(redes)
        music.play(1)
    if stop == 6:
        music.stop()
        for ips in nodos:
            redes = red + ips + "/?musica=stop"
            http_get(redes)
    response = web_page(nombre,color)
    conn.send('HTTP/1.1 200 OK\n')
    conn.send('Content-Type: text/html\n')
    conn.send('Connection: close\n\n')
    conn.sendall(response)
    conn.close()
