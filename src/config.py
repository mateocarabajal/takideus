ssid = "nombre_del_router_wifi" #aca va el nombre de la red de wifi
password = "contraseña_del_router" # esta es la contraseña
red = "http://192.168.100."  # 3 primeros numeros de ip de la red
nodos = []# numeros de ip de cada nodo
nombre = "parada 1- terminal 1" #nombre
color = "#FF3300" #color identificador para la web

""""
#######################################################################

En este archivo configuramos las variables del nodo central y los nodos 
perifericos.

ssid : es el nombre del router wifi a donde va a apuntar el nodemcu
password : el pass
red : Es importante notar que tiene un punto al final, si no lo ponen
      va a dar error, tipicamente una red de wifi puede ser: 
      'http://192.168.100.10', aca tomamos  hasta el ultimo punto
      dejando el numero para la variable nodos
nodos : contiene los numero de los IPs de las otras placas que van a sonar
        si la terminal que se esta configurando es solamente un nodo
        periferico, dejar la lista vacia [] para que no de error.
        si es un nodo central, poner los numero (en formato str) de cada
        terminal.
        por ej:
        
        Si la red es 192.168.0/255, el nodo central es 192.168.0.10
        y los nodos terminales: 192.168.0.11 y 192.168.0.12
        la conf deberia ser:
        
        #### config.py del nodo maestro
        red = 'http://192.168.0."
        nodos = ['11','12']
        
        #### config.py de los nodos terminal
        red = 'http://192.168.0."
        nodos = []

nombre: esta variable (str) tiene el nombre que se mostrara en la pantalla
        OLED cuado se incia, y en la página web a la que se acceda
color: (str) valor en RGB para pintar los botones de la WEB
            
"""
